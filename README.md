# OpenVMS Version 7.3 Documentation

This is my cache of documentation for the OpenVMS version 7.3 operating system,
gathered here for preservation and convenience. This is the last version of the
VMS operating system series with support for the VAX architecture. This
documentation was formerly provided to OpenVMS Hobbyist Program users. I have
organized it into a few categories, indexed and sorted by title, and highlighted
several important manuals to make it all a little bit easier to wade through.


## Cover Letter and Master Index

Title                                                             | File
------------------------------------------------------------------|------
Cover Letter for Compaq OpenVMS Alpha and OpenVMS VAX Version 7.3 | [AV-QSBVH-TET1.pdf](AV-QSBVH-TET1.pdf)
OpenVMS Master Index                                              | [OVMS_73_MASTER_INDEX.PDF](OVMS_73_MASTER_INDEX.PDF)


## Installation Manuals

Title                                                       | File
------------------------------------------------------------|------
OpenVMS Alpha Version 7.3 Upgrade and Installation Manual   | [Installation/OVMS_73_alpha_install.PDF](Installation/OVMS_73_alpha_install.PDF)
**OpenVMS VAX Version 7.3 Upgrade and Installation Manual** | [Installation/OVMS_73_VAX_INSTALL.pdf](Installation/OVMS_73_VAX_INSTALL.pdf)
OpenVMS Version 7.3 New Features and Documentation Overview | [Installation/OVMS_73_NEW_FEATURES.PDF](Installation/OVMS_73_NEW_FEATURES.PDF)
OpenVMS Version 7.3 Release Notes                           | [Installation/OVMS_73_REL_NOTES.pdf](Installation/OVMS_73_REL_NOTES.pdf)


## User Manuals

Title                                                           | File
----------------------------------------------------------------|------
DEC Text Processing Utility Reference Manual                    | [User/OVMS_73_DEC_TPU_REF.PDF](User/OVMS_73_DEC_TPU_REF.PDF)
Extensible Versatile Editor Reference Manual                    | [User/OVMS_73_EVE.PDF](User/OVMS_73_EVE.PDF)
Guide to the DEC Text Processing Utility                        | [User/OVMS_73_GD_DEC_TPU.PDF](User/OVMS_73_GD_DEC_TPU.PDF)
**OpenVMS DCL Dictionary: A–M**                                 | [User/OVMS_73_dcldict_ref1.PDF](User/OVMS_73_dcldict_ref1.PDF)
**OpenVMS DCL Dictionary: N–Z**                                 | [User/OVMS_73_dcldict_ref2.PDF](User/OVMS_73_dcldict_ref2.PDF)
OpenVMS Record Management Utilities Reference Manual            | [User/OVMS_73_REC_MAN_UTIL_REF.PDF](User/OVMS_73_REC_MAN_UTIL_REF.PDF)
OpenVMS System Messages: Companion Guide for Help Message Users | [User/OVMS_73_SYS_MES.pdf](User/OVMS_73_SYS_MES.pdf)
**OpenVMS User’s Manual**                                       | [User/OVMS_73_USER.pdf](User/OVMS_73_USER.pdf)


## System Management Manuals

Title                                                                                  | File
---------------------------------------------------------------------------------------|------
Availability Manager User’s Guide                                                      | [SystemManagement/ovms_73_avail_man.pdf](SystemManagement/ovms_73_avail_man.pdf)
DECamds User’s Guide                                                                   | [SystemManagement/OVMS_73_DECAMDS.PDF](SystemManagement/OVMS_73_DECAMDS.PDF)
Guidelines for OpenVMS Cluster Configurations                                          | [SystemManagement/OVMS_73_CLUSTER_CONFIG.PDF](SystemManagement/OVMS_73_CLUSTER_CONFIG.PDF)
OpenVMS Alpha Partitioning and Galaxy Guide                                            | [SystemManagement/ovms_73_galaxy_gd.pdf](SystemManagement/ovms_73_galaxy_gd.pdf)
OpenVMS Alpha System Analysis Tools Manual                                             | [SystemManagement/ovms_73_alpha_sys_tools.pdf](SystemManagement/ovms_73_alpha_sys_tools.pdf)
OpenVMS Cluster Systems                                                                | [SystemManagement/OVMS_73_CLUSTERs.PDF](SystemManagement/OVMS_73_CLUSTERs.PDF)
OpenVMS Guide to Extended File Specifications                                          | [SystemManagement/OVMS_73_EFS_GD.PDF](SystemManagement/OVMS_73_EFS_GD.PDF)
OpenVMS Guide to System Security                                                       | [SystemManagement/ovms_73_sys_sec.pdf](SystemManagement/ovms_73_sys_sec.pdf)
OpenVMS License Management Utility Manual                                              | [SystemManagement/OVMS_73_LMU.PDF](SystemManagement/OVMS_73_LMU.PDF)
OpenVMS Management Station Overview and Release Notes                                  | [SystemManagement/OVMS_73_MANG_ST.PDF](SystemManagement/OVMS_73_MANG_ST.PDF)
OpenVMS Performance Management                                                         | [SystemManagement/OVMS_73_perf_mang.PDF](SystemManagement/OVMS_73_perf_mang.PDF)
**OpenVMS System Management Utilities Reference Manual: A–L**                          | [SystemManagement/OVMS_73_sys_util_ref1.PDF](SystemManagement/OVMS_73_sys_util_ref1.PDF)
**OpenVMS System Management Utilities Reference Manual: M–Z**                          | [SystemManagement/OVMS_73_sys_util_ref2.PDF](SystemManagement/OVMS_73_sys_util_ref2.PDF)
**OpenVMS System Manager’s Manual, Volume 1: Essentials**                              | [SystemManagement/OVMS_73_sysman1.PDF](SystemManagement/OVMS_73_sysman1.PDF)
**OpenVMS System Manager’s Manual, Volume 2: Tuning, Monitoring, and Complex Systems** | [SystemManagement/OVMS_73_sysman2.PDF](SystemManagement/OVMS_73_sysman2.PDF)
OpenVMS VAX System Dump Analyzer Utility Manual                                        | [SystemManagement/OVMS_73_VAX_SDA.pdf](SystemManagement/OVMS_73_VAX_SDA.pdf)
POLYCENTER Software Installation Utility Developer’s Guide                             | [SystemManagement/OVMS_73_PCSI_GD.pdf](SystemManagement/OVMS_73_PCSI_GD.pdf)
RMS Journaling for OpenVMS Manual                                                      | [SystemManagement/rms_journaling.pdf](SystemManagement/rms_journaling.pdf)
Volume Shadowing for OpenVMS                                                           | [SystemManagement/OVMS_73_VOL_SHAD.pdf](SystemManagement/OVMS_73_VOL_SHAD.pdf)


## Developer Manuals

Title                                                               | File
--------------------------------------------------------------------|------
Compaq C Run-Time Library Utilities Reference Manual                | [Developer/OVMS_73_C_RUNTIME.PDF](Developer/OVMS_73_C_RUNTIME.PDF)
Compaq Portable Mathematics Library                                 | [Developer/OVMS_73_CPML.PDF](Developer/OVMS_73_CPML.PDF)
Guide to Creating OpenVMS Modular Procedures                        | [Developer/OVMS_73_mod_proc.PDF](Developer/OVMS_73_mod_proc.PDF)
Guide to OpenVMS File Applications                                  | [Developer/OVMS_73_file_app.PDF](Developer/OVMS_73_file_app.PDF)
Guide to the POSIX Threads Library                                  | [Developer/OVMS_73_posix_threads.PDF](Developer/OVMS_73_posix_threads.PDF)
HP C Run-Time Library Reference Manual for OpenVMS Systems          | [Developer/aa-rsmub-te.PDF](Developer/aa-rsmub-te.PDF)
OpenVMS Alpha Guide to Upgrading Privileged-Code Applications       | [Developer/ovms_73_priv_cd.pdf](Developer/ovms_73_priv_cd.pdf)
OpenVMS Calling Standard                                            | [Developer/ovms_73_call_stand.pdf](Developer/ovms_73_call_stand.pdf)
OpenVMS Command Definition, Librarian, and Message Utilities Manual | [Developer/OVMS_73_CDLM_UTIL.PDF](Developer/OVMS_73_CDLM_UTIL.PDF)
OpenVMS Connectivity Developer Guide                                | [Developer/ovms_73_connect_dev_gd.pdf](Developer/ovms_73_connect_dev_gd.pdf)
OpenVMS Debugger Manual                                             | [Developer/OVMS_73_DEBUGGER.PDF](Developer/OVMS_73_DEBUGGER.PDF)
OpenVMS Delta/XDelta Debugger Manual                                | [Developer/OVMS_73_DELTA_XDELTA.PDF](Developer/OVMS_73_DELTA_XDELTA.PDF)
OpenVMS I/O User’s Reference Manual                                 | [Developer/OVMS_73_IO_REF.PDF](Developer/OVMS_73_IO_REF.PDF)
OpenVMS Linker Utility Manual                                       | [Developer/OVMS_73_LINKER_UTIL.PDF](Developer/OVMS_73_LINKER_UTIL.PDF)
OpenVMS MACRO-32 Porting and User’s Guide                           | [Developer/OVMS_73_MACRO32_port.PDF](Developer/OVMS_73_MACRO32_port.PDF)
OpenVMS Programming Concepts Manual, Volume I                       | [Developer/OVMS_73_PROG_CON1.PDF](Developer/OVMS_73_PROG_CON1.PDF)
OpenVMS Programming Concepts Manual, Volume II                      | [Developer/OVMS_73_PROG_CON2.PDF](Developer/OVMS_73_PROG_CON2.PDF)
OpenVMS Record Management Services Reference Manual                 | [Developer/OVMS_73_REC_MAN_SERV.PDF](Developer/OVMS_73_REC_MAN_SERV.PDF)
OpenVMS RTL General Purpose (OTS$) Manual                           | [Developer/OVMS_73_rtl_ots.PDF](Developer/OVMS_73_rtl_ots.PDF)
OpenVMS RTL Library (LIB$) Manual                                   | [Developer/OVMS_73_RTL_LIB.PDF](Developer/OVMS_73_RTL_LIB.PDF)
OpenVMS RTL Screen Management (SMG$) Manual                         | [Developer/OVMS_73_RTL_SMG.PDF](Developer/OVMS_73_RTL_SMG.PDF)
OpenVMS RTL String Manipulation (STR$) Manual                       | [Developer/OVMS_73_RTL_STR.PDF](Developer/OVMS_73_RTL_STR.PDF)
OpenVMS System Services Reference Manual: A–GETUAI                  | [Developer/OVMS_73_sysserv_ref1.pdf](Developer/OVMS_73_sysserv_ref1.pdf)
OpenVMS System Services Reference Manual: GETUTC–Z                  | [Developer/OVMS_73_sysserv_ref2.pdf](Developer/OVMS_73_sysserv_ref2.pdf)
OpenVMS Utility Routines Manual                                     | [Developer/OVMS_73_UTIL_ROUTINES.pdf](Developer/OVMS_73_UTIL_ROUTINES.pdf)
OpenVMS VAX RTL Mathematics (MTH$) Manual                           | [Developer/OVMS_73_RTL_MTH.pdf](Developer/OVMS_73_RTL_MTH.pdf)
VAX MACRO and Instruction Set Reference Manual                      | [Developer/OVMS_73_VAX_MACRO_REF.pdf](Developer/OVMS_73_VAX_MACRO_REF.pdf)
